﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouterScanUpdater
{
    public partial class Form1 : Form
    {
        List<string> _7zipDefaultFolders = new List<string>() {
            @".\",
            @"C:\Program Files\7-Zip",
            @"C:\Program Files (x86)\7-Zip"
        };

        
        string pathTo7zip = "";
        string pathToRS   = "";

        string configFile = "config.ini";

        List<ZipFileInfo> toUpdate = new List<ZipFileInfo>();

        public Form1()
        {
            InitializeComponent();
            buttonUpdate.Enabled = false;
        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
            toUpdate.Clear();
            downloadFile("https://msk1.stascorp.com/routerscan/prerelease.7z", "./downloads", "prerelease.7z");
        }

        private void downloadFile(string url, string path, string fileName)
        {
            textBoxLog.AppendText("Downloading file..." + Environment.NewLine);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (!path.EndsWith(@"\") && !path.EndsWith("//"))
            { 
                path += @"\"; 
            }

            if (File.Exists(path + fileName) && false)
            {
                File.Delete(path + fileName);
            }

            Uri URL = new Uri(url);
            WebClient myWebClient = new WebClient();

            try
            {
                myWebClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);
                myWebClient.DownloadProgressChanged += MyWebClient_DownloadProgressChanged;
                myWebClient.DownloadFileAsync(URL, path + fileName);
            }
            catch (Exception ex)
            {
                progressBarDownload.Value = 0;
                labelDownloadStatus.Text = "-";
                textBoxLog.AppendText(ex.Message + Environment.NewLine);
            }
        }

        private void MyWebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            labelDownloadStatus.Text = $"{e.BytesReceived}/{e.TotalBytesToReceive}"; 
            progressBarDownload.Value = e.ProgressPercentage;
        }

        private async void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            progressBarDownload.Value = 0;
            labelDownloadStatus.Text = "-";

            if (e.Cancelled)
            {
                textBoxLog.AppendText(e.Error.Message + Environment.NewLine);
                return;
            }

            textBoxLog.AppendText("File downloaded. Checking..." + Environment.NewLine);
            await Task.Factory.StartNew(() => { System.Threading.Thread.Sleep(1000); });
            Open7Zip();
        }

        private void Open7Zip()
        {
            progressBarDownload.Value = 0;
            Process process = new Process()
            {
                StartInfo = new ProcessStartInfo($@"{pathTo7zip}\7z", $@"l ./downloads/prerelease.7z"),
            };

            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();

            StreamReader reader = process.StandardOutput;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                
                while (line.IndexOf("  ") != -1)
                {
                    line = line.Replace("  ", " ");
                }
                List<string> words = line.Split(' ').ToList();
                if (words.Count > 0 && DateTime.TryParse(words[0], out DateTime date))
                {
                    ZipFileInfo fileInfo = new ZipFileInfo();
                    string d = $"{words[0]} {words[1]}";
                    fileInfo.dateTime = DateTime.Parse(d);
                    fileInfo.isFolder = (words.Count == 6 && words[3] == "0");
                    fileInfo.Size = words[3];
                    fileInfo.Name = words.Last();
                    if (fileInfo.Name == "folders")
                    {
                        continue;
                    }
                    string text = "[-]";
                    long len = 0;
                    if (fileInfo.isFolder)
                    {
                        if (Directory.Exists($"{pathToRS}/{fileInfo.Name}" ))
                        {
                            text = "[+]"; 
                        }
                    }
                    else
                    {
                        if (File.Exists($"{pathToRS}/{fileInfo.Name}"))
                        {
                            text = "[+]";
                            len = new FileInfo($"{pathToRS}/{fileInfo.Name}").Length;
                            if (len != long.Parse(fileInfo.Size) && !fileInfo.Name.EndsWith(".txt"))
                            {
                                text = "[U]";
                                toUpdate.Add(fileInfo);                             
                            }
                        }
                    }
                    if (text == "[-]")
                    {
                        toUpdate.Add(fileInfo);
                    }
                    textBoxLog.AppendText(text + ' ' + fileInfo + Environment.NewLine);
                }
            }
            buttonUpdate.Enabled = toUpdate.Count > 0;
            if (buttonUpdate.Enabled)
            {
                textBoxLog.AppendText($"Done. Ready for update: {toUpdate.Count} file/s." + Environment.NewLine);
            }
            else
            {
                textBoxLog.AppendText($"Done. Nothing to update." + Environment.NewLine);
            }
            if (!process.HasExited)
            {
                process.Kill();
            }
            process.WaitForExit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(configFile))
            {
                List<string> config = File.ReadAllLines(configFile).ToList();
                if (config.Count != 2)
                {
                    File.Delete(configFile);
                    MessageBox.Show("Wrong ini file");
                    Application.Exit();
                }
                else
                {
                    if (Directory.Exists(config[0]))
                    {
                        if (File.Exists($"{config[0]}/7z.exe"))
                        {
                            pathTo7zip = config[0];
                        }
                        else
                        {
                            File.Delete(configFile);
                            MessageBox.Show("Wrong ini file");
                            Application.Exit();
                        }
                    }
                    else
                    {
                        File.Delete(configFile);
                        MessageBox.Show("Wrong ini file");
                        Application.Exit();
                    }
                    if (Directory.Exists(config[1]))
                    {
                        pathToRS = config[1];
                    }
                    else
                    {
                        File.Delete(configFile);
                        MessageBox.Show("Wrong ini file");
                        Application.Exit();
                    }
                }
            }
            else
            {
                bool picked = false;
                foreach (string path in _7zipDefaultFolders)
                {
                    if (Directory.Exists(path) && File.Exists(path + "/7z.exe"))
                    {
                        pathTo7zip = path;
                        picked = true; 
                        break; 
                    }
                }
                while (!picked)
                {
                    FolderBrowserDialog dialog = new FolderBrowserDialog()
                    {
                        Description = "Pick 7-Zip folder"
                    };
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        if (File.Exists(dialog.SelectedPath + "/7z.exe"))
                        {
                            pathTo7zip = dialog.SelectedPath;
                            picked = true;
                        }
                        else
                        {
                            picked = false;
                        }
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
                picked = false;
                while (!picked)
                {
                    FolderBrowserDialog dialog = new FolderBrowserDialog()
                    {
                        Description = "Pick RouterScan folder (May be empty)",
                    };
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        pathToRS = dialog.SelectedPath;
                        picked = true;
                    }
                    else
                    {
                        Application.Exit();
                    }
                }

                List<string> config = new List<string>() { pathTo7zip, pathToRS };

                File.WriteAllLines(configFile, config);
            }
        }

        public class ZipFileInfo
        { 
            public DateTime dateTime { get; set; }
            public string Size { get; set; }
            public string Name { get; set; }
            public bool isFolder { get; set; }

            public override string ToString()
            {
                return $"[{dateTime}]\tName:{Name}";
            }
        }

        private void progressBarDownload_Click(object sender, EventArgs e)
        {

        }

        private async void buttonUpdate_Click(object sender, EventArgs e)
        {
            buttonUpdate.Enabled = false;
            foreach (ZipFileInfo zipFile in toUpdate)
            {
                if (zipFile.isFolder)
                {
                    if (!Directory.Exists($"{pathToRS}/{zipFile.Name}"))
                    {
                        Directory.CreateDirectory($"{pathToRS}/{zipFile.Name}");
                    }
                    continue;
                }
                textBoxLog.AppendText($"Updating {zipFile.Name}...{Environment.NewLine}");
                string result = await Task<string>.Factory.StartNew(() => {
                    if (File.Exists($"{pathToRS}/{zipFile.Name}"))
                    {
                        File.Delete($"{pathToRS}/{zipFile.Name}");
                    }
                    string commandLine = $@"x ./downloads/prerelease.7z -o{FixPath(pathToRS)} {zipFile.Name} -r";
                    
                    Process process = new Process()
                    {
                        StartInfo = new ProcessStartInfo($@"{pathTo7zip}\7z", commandLine),
                    };

                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.Start();
                    StreamReader reader = process.StandardOutput;
                    string r = reader.ReadToEnd();
                    if (!process.HasExited)
                    {
                        process.Kill();
                    }
                    process.WaitForExit();
                    return r;
                });
                string opreratioonResult = result.Contains("Everything is Ok") ? "Success " : "Fail";
                textBoxLog.AppendText($"Result:{opreratioonResult}" + Environment.NewLine);
                
            }
            textBoxLog.AppendText("Done, all updated" + Environment.NewLine);
            toUpdate.Clear();
        }

        public string FixPath(string path)
        {
            string fix = "";

            List<string> dirs = path.Split('\\').ToList();

            foreach (string dir in dirs)
            {
                if (dir.Contains(' '))
                {
                    fix += '"' + dir + '"' + '\\';
                }
                else
                {
                    fix += dir + '\\';
                }
            }

            return fix;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (File.Exists($"{pathToRS}\\RouterScan.exe"))
            {
                Process.Start($"{pathToRS}\\RouterScan.exe");
                Close(); 
            }
        }
    }
}
